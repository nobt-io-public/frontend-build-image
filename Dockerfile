FROM mhart/alpine-node

RUN apk add --update openssl bash libstdc++ curl && \
    rm -rf /var/cache/apk/* && \
    wget "https://cli.run.pivotal.io/stable?release=linux64-binary&version=6.22.1" -O cf-cli.tgz && \
    tar -zxf cf-cli.tgz -C /usr/local/bin && \
    rm cf-cli.tgz